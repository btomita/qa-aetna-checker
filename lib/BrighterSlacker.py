"""
Library to communicate with Brighter Slack.
"""
from slacker import Slacker
import re
import random
import time


class BrighterSlacker(object):

    def __init__(self, token):
        self.__token = token
        self.slack = Slacker(self.__token)

    def get_matching_appointment_request_sms(self, regexp, oldest=0):
        """
        Searches through messages from the bot user, 'IFTTT' made in the '#telephone-app-sms' channel.

        Fails if no matching SMS can be found.

        :param regexp: the regular expression to match
        :param oldest: start of time range of messages to include (e.g. 1474489260)
        :return: a list of matching messages
        """
        print "*INFO* Searching channel, #telephone-app-sms, for matching messages from 'IFTTT' bot."

        history = self.slack.channels.history("C261DPDM1", oldest=int(oldest)).body['messages']
        print "Found " + str(len(history)) + " messages since " + str(oldest)

        if len(history):
            print "Looking for messages matching regular expression: " + "\n'" + regexp + "'"
            messages = [message['attachments'][0]['text'] for message in history
                        if message.get('username') == "IFTTT"
                        and re.search(regexp, message.get('attachments')[0].get('text'))]
        else:
            messages = []

        if len(messages) == 0:
            raise AssertionError("No matching SMS found.")
        elif len(messages) == 1:
            print "Found one matching message."
        elif len(messages) > 1:
            print "*WARN* Multiple matching SMS messages found."

        return messages

    def post_slack_message(self, message, channel="#qa-automation"):
        """
        Posts the given message to the channel.
        :param message: The message to post
        :param channel: The channel to post to
        :return:
        """
        return self.slack.chat.post_message(channel, message, as_user=True, link_names=1)

    def is_member_lite_rebuild_running(self):
        """
        Checks the #member-staging-events channel to determine if Member Lite Rebuild is running or not.
        :return: True, False, or None if cannot be determined
        """
        channel_id = self.slack.channels.get_channel_id("member-staging-events")
        messages = self.slack.channels.history(channel_id).body['messages']

        is_running = None

        # LOGIC:
        # Member Lite Rebuild triggers three main Slack messages in this order
        # 1.  Starting Full Member Lite Build.
        # 2.  Starting Full Utilization Lite Build.
        # 3.  Starting Full Member Cache Build.
        # After 3, it can be ran again, though some accounts may not have been updated if there were active sessions
        # while 1 or 2 were running.  This should not be a problem as long as we run this in the teardown of scripts.
        for message in messages:
            if (message.get('username') == 'incoming-webhook'
                    and re.match(r"Starting (?:Full|Delta) Member Cache Build", message['text'])):
                print "Member Lite Rebuild is not running."
                is_running = False
                break
            elif (message.get('username') == 'incoming-webhook'
                  and (re.match(r"Starting (?:Full|Delta) Utilization Lite Build", message['text'])
                       or re.match(r"Starting (?:Full|Delta) Member Lite Build", message['text']))):
                print "Member Lite Rebuild is running."
                is_running = True
                break
            else:
                pass

        return is_running


