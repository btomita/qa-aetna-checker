*** Settings ***
Documentation
Library     SeleniumLibrary    run_on_failure=NONE
Library     XML
Library     OperatingSystem
Library     ../lib/BrighterSlacker.py    xoxb-142102148181-oyZ7VxxGJUeeWV3XzvDtnKcP
Resource    ../pages/AetnaSharedComponents.robot
Resource    ../pages/AetnaLoginPage.robot
Resource    ../pages/AetnaDashboardPage.robot
Resource    ../pages/AetnaRedirectDisclaimerPage.robot

Resource    ../pages/BrighterSharedComponents.robot
Resource    ../pages/BrighterFindADentistPage.robot

Suite Setup     Go To Aetna Login Page Setup
Suite Teardown  Notify Slack Teardown

*** Variables ***
${PREV_RUN_STATUS}
${ERROR_MESSAGE}
${STATUS_FILE_PATH}    ${OUTPUT DIR}${/}previous_run_status.txt
${SLACK_CHANNEL}    \#aetna-commercial-dev

*** Test Cases ***
Login To Aetna Navigator
    [Documentation]    User should be able to login to Aetna.
    [Teardown]    Set Suite Variable    ${ERROR_MESSAGE}    ${ERROR_MESSAGE}${\n}${TEST_NAME} - *${TEST_STATUS}*
    Submit Aetna Nav Login Form
    ...    ${CONFIG['accounts']['default']['username']}
    ...    ${CONFIG['accounts']['default']['password']}
    Wait Until Keyword Succeeds    60s    5s
    ...    Location Should Contain    page=homePageNew
    Wait Until Loading Spinner Is Not Visible    5s
    Dismiss Tax Form Popup
    Sleep    3s    Allow the fade modal to disappear.

Redirect To Brighter
    [Documentation]    User should be able to redirect to Brighter from Aetna
    ...    dashboard.
    [Teardown]    Set Suite Variable    ${ERROR_MESSAGE}    ${ERROR_MESSAGE}${\n}${TEST_NAME} - *${TEST_STATUS}*
    Wait Until Keyword Succeeds    30s    5s    Click Find Care Dentist Link
    Wait Until Keyword Succeeds    10s    2s    Select Window    new
    ${is_disclaimer_page} =    Run Keyword And Return Status
    ...    Wait Until Keyword Succeeds    15s    2s
    ...    Title Should Be    Disclaimer
    Run Keyword If    ${is_disclaimer_page}==${TRUE}
    ...    Sleep    3s    Allow page content to load fully.
    Run Keyword If    ${is_disclaimer_page}==${TRUE}
    ...    Wait Until Keyword Succeeds    10s    2s
    ...    Click Continue For Aetna Disclaimer Page
    Wait Until Keyword Succeeds    20s    2s
    ...    Location Should Contain    aetnadental.com/account
    Wait Until Keyword Succeeds    20s    2s
    ...    Carrier Header Should Be Visible
    Sleep    1s    Sleep to ensure content has loaded.

Search For A Dentist
    [Documentation]    User should be able to search for a dentist.
    [Teardown]    Set Suite Variable    ${ERROR_MESSAGE}    ${ERROR_MESSAGE}${\n}${TEST_NAME} - *${TEST_STATUS}*
    Click Top Navigator Link    Find a Dentist
    Click Search Button For Dentist Search

*** Keywords ***
Go To Aetna Login Page Setup
    [Documentation]    Go to the Aetna login page.
    # notify slack that it is running
    [Teardown]    Set Suite Variable    ${ERROR_MESSAGE}    ${ERROR_MESSAGE}${\n}Go To Aetna Login Page - *${KEYWORD_STATUS}*
    Post Slack Message
    ...    message=Checking to see if Aetna (${CONFIG['environment']}) is up...
    ...    channel=\#qa-slackbots

    Open Browser    url=about:blank    browser=${CONFIG['browser']}
    Maximize Browser Window
    Wait Until Keyword Succeeds    25s    3s
    ...    Go To Aetna Navigator Login Page

Notify Slack Teardown
    [Documentation]    Closes the browser and notifies Slack if there has been
    ...    a change in status.
    Close All Browsers
    # parse status of last run
    ${status_file_exists} =    Run Keyword And Return Status
    ...    File Should Exist    ${STATUS_FILE_PATH}
    ${prev_status} =    Run Keyword If    ${status_file_exists}==${TRUE}
    ...    Get File    ${STATUS_FILE_PATH}
    # notify slack if there was a change
    ${slack_message} =    Catenate    SEPARATOR=${\n}
    ...    *Is Aetna Up?* ¯\\_(ツ)_/¯
    ...    Environment: *${CONFIG['environment']}*
    ...    Status: *${SUITE STATUS}*
    ...    Username: ${CONFIG['accounts']['default']['username']}
    ...    ${\n}${ERROR_MESSAGE.strip()}
    ${status_changed} =    Run Keyword And Return Status
    ...    Should Not Be Equal As Strings    ${prev_status}    ${slack_message}
    Run Keyword If    ${status_changed}==${TRUE}
    ...    Post Slack Message
    ...    message=${slack_message}
    ...    channel=${SLACK_CHANNEL}
    Create File    ${status_file_path}    ${slack_message}

