*** Settings ***
Documentation    https://qa2member.aetna.com/secure/member/?v7#/headerLayoutContentPage?page=standardDisclaimer&storeURL=https:%2F%2Fstaging-member.aetnadental.com%2Fauth%2Fcb%3Fiss%3Dhttps:%2F%2Fopenid.aetna.com%2Fconsumer%26login_hint%3DHoI0AvGZb31HgBYmCUMJHZZPmAEczr3KorHVnQ1%2FGFC6IlWupQq%2BuAAeLmDFRi0rdFJ2XEDHREvVdGLajmIDKw%3D%3D%26target_link_uri%3Dhttps:%2F%2Fstaging-member.aetnadental.com%2Faccount%2Fsearch
Library    SeleniumLibrary

*** Variables ***
${loc_aetnaNav_disclaimer_continueBtn}    css=#continue

*** Keywords ***
Click Continue For Aetna Disclaimer Page
    [Documentation]    Clicks the 'Continue' button on the Disclaimer page.
    Click Element    ${loc_aetnaNav_disclaimer_continueBtn}