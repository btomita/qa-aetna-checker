*** Settings ***
Documentation    Keywords for common components that are shared across
...    Aetna Navigator such as headers and footers.
Library    SeleniumLibrary

*** Variables ***
${loc_aetnaNav_shared_loadingImg}       css=.newNavSpinnerImg[alt='Loading Spinner']

*** Keywords ***
Wait Until Loading Spinner Is Not Visible
    [Arguments]   ${wait}=5s
    [Documentation]    Waits until the loading spinner image is not visible.
    Wait Until Keyword Succeeds    ${wait}    1s
    ...    Element Should Not Be Visible    ${loc_aetnaNav_shared_loadingImg}
