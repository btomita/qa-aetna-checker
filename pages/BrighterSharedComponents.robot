*** Settings ***
Documentation    Keywords for components that are reused across pages like navigation headers, footers, etc.
Library    SeleniumLibrary

*** Variables ***
${loc_aetna_carrierHeaderDiv}    css=#carrier-header
${loc_temp_aetna_topNavLinks}    xpath=.//ul[@id='nav-top-links']/li[contains(., '{ph_link}')]

*** Keywords ***
Carrier Header Should Be Visible
    [Documentation]    Fails if the Carrier Header is not visible.
    Element Should Be Visible    ${loc_aetna_carrierHeaderDiv}

Click Top Navigator Link
    [Arguments]    ${link_name}
    [Documentation]    Clicks the given link name from the Top Navigator.
    ...    Link names: Home, Find a Dentist, Claims, Email Settings
    Click Element    ${loc_temp_aetna_topNavLinks.format(ph_link='${link_name}')}