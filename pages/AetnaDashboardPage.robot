*** Settings ***
Documentation    https://qa2member.aetna.com/secure/member/?v7#/contentPage?page=homePageNew
Library    SeleniumLibrary

*** Variables ***
${loc_aetnaNav_home_taxFormPopup}       css=#taxDocPopup
${loc_aetnaNav_home_findCareTab_dentistLink}     css=a[href*='providerSearch'] a[href*='member.aetnadental']

*** Keywords ***
Dismiss Tax Form Popup
    [Documentation]    Dismisses the tax form popup if it is visible, otherwise does nothing.
    ${is_visible} =    Run Keyword And Return Status    Wait Until Element Is Visible
    ...    ${loc_aetnaNav_home_taxFormPopup}    3s
    Run Keyword If    ${is_visible}==${TRUE}    Press Key    ${loc_aetnaNav_home_taxFormPopup}    \\27
    Wait Until Element Is Not Visible    ${loc_aetnaNav_home_taxFormPopup}

Click Find Care Dentist Link
    [Documentation]    Clicks the Find Care > Dentist link.
    Click Element    ${loc_aetnaNav_home_findCareTab_dentistLink}