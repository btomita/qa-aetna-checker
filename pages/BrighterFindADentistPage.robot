*** Settings ***
Documentation    member.aetnadental.com/account/search
Library    SeleniumLibrary

*** Variables ***
${loc_aetna_dentistSearch_searchBtn}    css=#dentist-search input.btn-primary

*** Keywords ***
Click Search Button For Dentist Search
    [Documentation]    Clicks the 'Search' button on the dentist search page.
    Click Element    ${loc_aetna_dentistSearch_searchBtn}