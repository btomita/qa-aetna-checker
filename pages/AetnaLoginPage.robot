*** Settings ***
Documentation    https://member.aetna.com/appConfig/login/login.fcc
Library    SeleniumLibrary

*** Variables ***
${loc_aetnaNav_login_usernameInput}    css=#userName
${loc_aetnaNav_login_passwordInput}    css=#password
${loc_aetnaNav_login_loginBtn}         css=#secureLogin-btn

*** Keywords ***
Go To Aetna Navigator Login Page
    [Documentation]    Goes directly to the Aetna Navigator login url.]
    ${login_page} =    Set Variable If
    ...    "${CONFIG['environment'].lower()}" == "production"
    ...    https://member.aetna.com/appConfig/login/login.fcc
    ...    https://qa2member.aetna.com/appConfig/login/login.fcc
    Go To    ${login_page}
    Wait Until Keyword Succeeds    10s    2s
    ...    Location Should Contain    login/login.fcc
    Wait Until Element Is Visible    ${loc_aetnaNav_login_loginBtn}    3s

Submit Aetna Nav Login Form
    [Arguments]    ${username}    ${password}
    [Documentation]    Submits the given username and password.
    Input Text    ${loc_aetnaNav_login_usernameInput}    ${username}
    Input Password    ${loc_aetnaNav_login_passwordInput}    ${password}
    Click Element    ${loc_aetnaNav_login_loginBtn}