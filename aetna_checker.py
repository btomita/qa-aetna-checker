import argparse
import subprocess
import time

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config")
    parser.add_argument("--frequency")
    args = parser.parse_args()

    while(True):
        subprocess.call([
            "robot",
            "-A", str(args.config)])

        print('\n{ph_time}: Waiting {ph_seconds} seconds until the next run...\n'.format(
        	ph_seconds=str(args.frequency),
        	ph_time=time.strftime('%H:%M', time.localtime())
            ))

        time.sleep(int(args.frequency))
